(defproject learningpatterns-blog "0.1.0"
            :description "Research software engineering, systems administration, teaching"
            :url "http://learningpatterns.me/"
            :license {:name "Creative Commons License BY-NC-SA"
                      :url "https://creativecommons.org/licenses/by-nc-sa/4.0/"}
            :dependencies [[org.clojure/clojure "1.10.0"]
                           [ring/ring-devel "1.7.1"]
                           [compojure "1.6.1"]
                           [ring-server "0.5.0"]
                           [cryogen-markdown "0.1.11"]
                           [cryogen-core "0.1.68"]]
            :plugins [[lein-ring "0.12.5"]]
            :main cryogen.core
            :ring {:init cryogen.server/init
                   :handler cryogen.server/handler})
