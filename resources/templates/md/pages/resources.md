{:title "Resources"
 :layout :page
 :page-index 2
 :navbar? true}

* (Son of) Grid Engine [cheatsheet](https://gist.github.com/willfurnass/90847c3da83b93c8c02eecdaef2e521f)
* tmux [cheatsheet](https://gist.github.com/willfurnass/19588766e3f0145a49e4b3a38ec0801c)
* [qrshx](https://gist.github.com/willfurnass/10277756070c4f374e6149a281324841): 'qrsh for humans'
