{:title "High-performance Python workshop"
 :layout :post
 :tags  ["python", "hpc", "jupyter", "jupyterhub", "joblib", "mkl", "pyfftw", "numpy", "multithreading", "multiprocessing"]
 :toc true
 :draft? true
 }


Back on 2018-04-12

Roped in my (now ex) boss, [Mike Croucher][walkingrandomly] as a helper

To dicuss: 
learning outcomes
success?

resources used in the workshop


style of delivery

What would I change?

What next?


![Workshop participants](/img/odk-hi-perf-py-wshop.jpg)

[walkingrandomly]: http://www.walkingrandomly.com/
[joblib]: https://pythonhosted.org/joblib/
[mkl]: https://software.intel.com/en-us/mkl
[numpy]: http://www.numpy.org/
[pyfftw]: https://hgomersall.github.io/pyFFTW/
[soge]: https://arc.liv.ac.uk/trac/SGE
[odk]: https://opendreamkit.org/
[sharc]: http://docs.hpc.shef.ac.uk
[jh-sharc-docs]: http://docs.hpc.shef.ac.uk/en/latest/hpc/jupyterhub.html
[jh-sharc-repo]: https://github.com/RSE-Sheffield/jupyterhub-gridengine-sharc/

